from sqlalchemy import Column, UniqueConstraint
from sqlalchemy import Integer
from database import quoted_name

__author__ = 'joshua.morton'

def ATLANTA_GEO_DATA_WITH_DATES(Base):
    """
    example function to create a table
    :param Base: the Base class (Declarative Base)
    :return: a table class
    """
    class ATLANTA_GEO_DATA_WITH_DATES(Base):
        __tablename__ = "ATLANTA_GEO_DATA_WITH_DATES"  # the table name in the database
        # any schema the table is in
        __table_args__ = {"schema": quoted_name('dev_tables', True)}
        # any special columns (relationships, keys, restrictions) still need to be applied manually
        mi_prinx = Column(Integer, primary_key=True)

        # you can also override python's magic methods like str and repr
        def __repr__(self):
            return "<ATLANTA_GEO_DATA_WITH_DATES object of id: " + self.mi_prinx + ">"

    return ATLANTA_GEO_DATA_WITH_DATES
