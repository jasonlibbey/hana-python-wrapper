__author__ = 'joshua.morton'
from database import Database as DB
from flask import Flask, render_template
import tables

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    geo_data_query = db.session.query(ATLANTA_GEO_DATA_WITH_DATES.x, ATLANTA_GEO_DATA_WITH_DATES.y).distinct()
    locations = geo_data_query.filter(ATLANTA_GEO_DATA_WITH_DATES.product == "Cases").all()
    return render_template('index.html', locations=locations)

@app.route('/heat_map')
def heat_map():
    geo_data_query = db.session.query(ATLANTA_GEO_DATA_WITH_DATES.x, ATLANTA_GEO_DATA_WITH_DATES.y).distinct()
    locations = geo_data_query.filter(ATLANTA_GEO_DATA_WITH_DATES.product == "Cases").all()
    return render_template('heat_map.html', locations=locations)

@app.route('/active_map')
def active_map():
    geo_data_query = db.session.query(ATLANTA_GEO_DATA_WITH_DATES.x, ATLANTA_GEO_DATA_WITH_DATES.y).distinct()
    locations = geo_data_query.filter(ATLANTA_GEO_DATA_WITH_DATES.product == "Cases").limit(20)
    return render_template('active_map.html', locations=locations)

@app.route('/foundation')
def foundation():
    return render_template('foundation.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

if __name__ == "__main__":
    db = DB()
    ATLANTA_GEO_DATA_WITH_DATES = db.register_table(tables.ATLANTA_GEO_DATA_WITH_DATES)
    db.map_tables()

    with db.scope() as session:
        app.run()

# TODO: add in argparse and support for command line tooling
