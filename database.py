from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.elements import quoted_name

__author__ = 'joshua.morton'

"""
db wrapper api
"""

quoted_name = quoted_name

class Database(object):
    def __init__(self, username="bobj_svc", password="Admin1234", url="sap-hana-01.decisionfirst.local", port=30015):
        """

        :param username:
        :param password:
        :param url:
        :param port:
        :return: None
        """
        self.username = username
        self.password = password
        self.url = url
        self.port = port
        self.base = declarative_base(cls=DeferredReflection)
        self.connection_uri = "hana://" + username + ":" + password + "@" + url + ":" + str(port)
        self.sessionmaker = None
        self.tables = dict()
        self.connection = None
        self.session = None

    def __del__(self):
        if self.connection is not None:
            self.connection.close()

    def __getattr__(self, item):
        if item in self.tables:
            return self.tables[item]
        else:
            raise AttributeError


    def register_table(self, class_creator):
        """

        :param class_creator: a function that takes in a sqlalchemy base class and returns a sqlalchmey table
        :return:
        """
        new_table = class_creator(self.base)
        self.tables[new_table.__name__] = new_table
        return new_table

    def map_tables(self):
        engine = create_engine(self.connection_uri)
        self.connection = engine.connect()
        self.base.prepare(engine)
        self.sessionmaker = sessionmaker(bind=engine)

    @contextmanager
    def scope(self):
        """
        a contextmanager for the session, wraps the sessionmaker in a factory and does some magic to keep everything
        clean and nice

        Usage:
            with db.session_scope() as session:
                result = session.query(MyTable)...
            print result

        :return: sqlalchemy session object to be queried and used as needed
        """
        if self.sessionmaker is None:
            raise NoSessionError
        session = self.sessionmaker()
        self.session = session
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
            self.session = None


class NoSessionError(Exception):
    pass
